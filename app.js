var angular = require('angular');
var testController = require('./controlers/testCtrl');

var app = angular.module('app', []);

app.controller('testCtrl', testController);

module.exports = app;