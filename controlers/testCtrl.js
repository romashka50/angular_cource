module.exports = function ($scope) {
    var MAX_LEN = 20;
    var WARN_LENGTH = 10;

    $scope.name = 'World';
    $scope.population = 7000;
    $scope.countries = [
        {name: 'Ukraine', population: 49.2},
        {name: 'USA', population: 61.8}
    ];
    $scope.getName = function () {
        return $scope.name;
    };
    $scope.remaining = function () {
        return MAX_LEN - $scope.message.length;
    };
    $scope.hasValidLength = function () {
        return MAX_LEN >= $scope.message.length;
    };
    $scope.shouldWarn = function () {
        return $scope.remaining() < WARN_LENGTH;
    };

};
